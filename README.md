# File Sharing v3


## Build Setup  
  
```bash  
# install dependencies  
$ yarn install  
  
# serve with hot reload at localhost:3000  
$ yarn watch  
  
# build for production 
$ yarn build   
  
# generate static sprites file  
$ yarn sprites    
```  