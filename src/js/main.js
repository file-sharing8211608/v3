// accordion

// $('.faq__category').click(function () {
//     let $this = $(this);
//     $this.toggleClass('faq__category--active').next().stop(true, true).slideToggle('fast');
// })

// accordion


// tab

/*$('.tab-block__item').click(function (e) {
    e.preventDefault();
    let $this = $(this);
    let data_tab = $this.data('tab');
    let tab_item = $('.tab-body__item');
    $('.tab-block__item').removeClass('tab-block__item--active');
    $this.addClass('tab-block__item--active')
    tab_item.removeClass('tab-body__item--active')
    for (let i = 0; i < tab_item.length; i++) {
        if (tab_item.eq(i).data('tab') == data_tab){
            tab_item.eq(i).addClass('tab-body__item--active')
        }
    }
});*/

// tab

function mobileInnerHeight() {
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

window.addEventListener('resize', mobileInnerHeight);

mobileInnerHeight();

// close popup
$('.popup__close, .popup__blur').click(function () {
    $('.popup').removeClass('open')
})
// close popup

// check mobile
function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
// check mobile


// header fixed script

// $(window).scroll(function () {
//     let sticky = $('.header__main'),
//         scroll = $(window).scrollTop();
//
//     if (scroll >= 38) sticky.addClass('fixed');
//     else sticky.removeClass('fixed');
// });

// header fixed script

$(document).on('click', '.main__link__btn', function (e) {
    e.preventDefault();
    let $this = $(this);
    let msg = $this.data('url');
    let copy_msg = $this.find('.main__link__msg')
    let temp = $("<input class='copy-input'>");
    $("body").append(temp);
    temp.val(msg).select();
    document.execCommand("copy");
    temp.remove();
    copy_msg.addClass('hide');
    $('.top-bar-msg').addClass('active');
});